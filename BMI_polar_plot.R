BMI_polar_plot <- function(age_value, post_model){

result_df <- data.frame()

for (diff_between in c("OA-CTRL", "CTA-CTRL")){
  
  var_ <- "BMI"
  
  age_mean <- 70
  BMI_mean <- 25
  
  main_title <- paste(diff_between, "\n")
  seq_len <- 500
  
  
  if (diff_between == "CTA-CTRL"){
    sg <- c(2, 6, 1, 5)
    patho_ <- "CTA"
  } else if (diff_between == "OA-CTRL"){
    sg <- c(2, 4, 1, 3)
    patho_ <- "OA"
  }
  
    
  xlab <- "BMI [kg/m^2]"
  
  var_seq <- seq(from=15, to=40, length.out=seq_len)
  
  l1 <- list(kernel=1,
             BMI=var_seq,
             age=rep(age_value, seq_len),
             age_mean = 70,
             BMI_mean = 25)
  
  # select BMD at BMI = 17, 22, 27, 32
  at17 <- which(var_seq <= 17)
  at17 <- at17[length(at17)]
  
  at25 <- which(var_seq <= 25)
  at25 <- at25[length(at25)]
  
  at33 <- which(var_seq <= 33)
  at33 <- at33[length(at33)]
  
  BMI_idx <- c(at17, at25, at33)
  
  locs <- c(3, 2, 4, 1, 5, 8, 6, 7) + 16
  loc_labs <- c("Superior-Posterior", "Superior-Anterior",
                "Posterior-Superior", "Anterior-Superior",
                "Posterior-Inferior", "Anterior-Inferior",
                "Inferior-Posterior", "Inferior-Anterior")
  
  loc_lab_idx <- 1
  
  BMI_levels <- c(17, 25, 33) 
  
  
  for (loc in locs){
    
    sim_data <- c(l1, list(sex_group=sg[1],location=loc))
    bmd_ctrl <- custom_link(post_model, sim_data)
    sim_data <- c(l1, list(sex_group=sg[2],location=loc))
    bmd_patho <- custom_link(post_model, sim_data)
    bmd_diff_female <- zscore_to_abs_ST(bmd_patho, loc, raw_data, patho_) - zscore_to_abs_ST(bmd_ctrl, loc, raw_data, "CTRL")
    
    for (i in 1:length(BMI_levels)){
      
      female_current_df <- data.frame(
        BMI = BMI_levels[i],
        sex = "Female",
        loc =  loc_labs[loc_lab_idx],
        mean = round(PI(bmd_diff_female[,BMI_idx[i]], prob=0.00001)[1], digits=1),
        err_min = round(PI(bmd_diff_female[,BMI_idx[i]], prob=0.89)[1], digits=1),
        err_max = round(PI(bmd_diff_female[,BMI_idx[i]], prob=0.89)[2], digits=1),
        Group = diff_between
      )
      result_df <- rbind(result_df, female_current_df)
      
    }
    
    sim_data <- c(l1, list(sex_group=sg[3],location=loc))
    bmd_ctrl <- custom_link(post_model, sim_data)
    sim_data <- c(l1, list(sex_group=sg[4],location=loc))
    bmd_patho <- custom_link(post_model, sim_data)
    bmd_diff_male <- zscore_to_abs_ST(bmd_patho, loc, raw_data, patho_) - zscore_to_abs_ST(bmd_ctrl, loc, raw_data, "CTRL")
    
    for (i in 1:length(BMI_levels)){
      
      male_current_df <- data.frame(
        BMI = BMI_levels[i],
        sex = "Male",
        loc =  loc_labs[loc_lab_idx],
        mean = round(PI(bmd_diff_male[,BMI_idx[i]], prob=0.00001)[1], digits=1),
        err_min = round(PI(bmd_diff_male[,BMI_idx[i]], prob=0.89)[1], digits=1),
        err_max = round(PI(bmd_diff_male[,BMI_idx[i]], prob=0.89)[2], digits=1),
        Group = diff_between
      )
      result_df <- rbind(result_df, male_current_df)
      
    }
    
    loc_lab_idx <- loc_lab_idx + 1
    
  }
  
}

result_df$Group <- factor(result_df$Group, levels=c("OA-CTRL", "CTA-CTRL"))
result_df$BMI <- paste("BMI:", result_df$BMI, "kg/m^2")
result_df$loc <- factor(result_df$loc, 
                        levels=c("Anterior-Inferior",
                                 "Inferior-Anterior",
                                 "Inferior-Posterior",
                                 "Posterior-Inferior",
                                 "Posterior-Superior",
                                 "Superior-Posterior",
                                 "Superior-Anterior",
                                 "Anterior-Superior"
                        ))

x_segment <- seq(0.5, length(unique(result_df$loc)), by = 1)
y_start_segment <- -200
y_end_segment <- 600
y_text_increase <- 100

p <- result_df %>%
  ggplot(aes(group=Group)) +
  geom_segment(x=-Inf, xend=Inf,
               y=-200, yend=-200,
               colour = "gray", linetype=3, alpha=0.4, size=0.3) +
  geom_segment(x=-Inf, xend=Inf,
               y=-100, yend=-100,
               colour = "gray", linetype=3, alpha=0.4, size=0.3) +
  geom_segment(x=-Inf, xend=Inf,
               y=0, yend=0,
               colour = "black", linetype=1, alpha=0.4, size=0.6) +
  geom_segment(x=-Inf, xend=Inf,
               y=100, yend=100,
               colour = "gray", linetype=3, alpha=0.4, size=0.3) +
  geom_segment(x=-Inf, xend=Inf,
               y=200, yend=200,
               colour = "gray", linetype=3, alpha=0.4, size=0.3) +
  geom_segment(x=-Inf, xend=Inf,
               y=300, yend=300,
               colour = "gray", linetype=3, alpha=0.4, size=0.3) +
  geom_segment(x=-Inf, xend=Inf,
               y=400, yend=400,
               colour = "gray", linetype=3, alpha=0.4, size=0.3) +
  geom_segment(x=-Inf, xend=Inf,
               y=500, yend=500,
               colour = "gray", linetype=3, alpha=0.4, size=0.3) +
  geom_segment(x=-Inf, xend=Inf,
               y=600, yend=600,
               colour = "gray", linetype=3, alpha=0.4, size=0.3) +
  geom_bar(aes(x=loc, y=mean, fill=Group),
           stat = "identity", position=position_dodge(0.9),
           width=0.7) +
  geom_errorbar(aes(x=loc, ymin=err_min, ymax=err_max),
                stat="identity", width=0.25, position=position_dodge(0.9)) +
  theme_minimal() +
  theme(
    legend.position = c(0, 1),
    legend.title=element_blank(),
    axis.text = element_blank(),
    axis.title = element_blank(),
    strip.text.y = element_markdown(angle=90),
    strip.text.x = element_text(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.grid.major.y = element_blank(),
    panel.spacing = unit(1, "lines"),
    plot.margin = unit(rep(1, 4), "cm") 
  ) +
  ylim(c(-400, 600)) +
  scale_fill_manual("Group",
                    values=c("darkred", "dodgerblue4"),
                    labels=c("OA-CTRL", "CTA-CTRL")) +
  geom_segment(x=x_segment[1], xend=x_segment[1],
               y=y_start_segment, yend=y_end_segment,
               colour = "black", linetype=1, alpha=0.6, size=0.3) +
  geom_segment(x=x_segment[2], xend=x_segment[2],
               y=y_start_segment, yend=y_end_segment,
               colour = "black", linetype=1, alpha=0.6, size=0.3) +
  geom_segment(x=x_segment[3], xend=x_segment[3],
               y=y_start_segment, yend=y_end_segment,
               colour = "black", linetype=1, alpha=0.6, size=0.3) +
  geom_segment(x=x_segment[4], xend=x_segment[4],
               y=y_start_segment, yend=y_end_segment,
               colour = "black", linetype=1, alpha=0.6, size=0.3) +
  geom_segment(x=x_segment[5], xend=x_segment[5],
               y=y_start_segment, yend=y_end_segment,
               colour = "black", linetype=1, alpha=0.6, size=0.3) +
  geom_segment(x=x_segment[6], xend=x_segment[6],
               y=y_start_segment, yend=y_end_segment,
               colour = "black", linetype=1, alpha=0.6, size=0.3) +
  geom_segment(x=x_segment[7], xend=x_segment[7],
               y=y_start_segment, yend=y_end_segment,
               colour = "black", linetype=1, alpha=0.6, size=0.3) +
  geom_segment(x=x_segment[8], xend=x_segment[8],
               y=y_start_segment, yend=y_end_segment,
               colour = "black", linetype=1, alpha=0.6, size=0.3) +
  geom_text(x=x_segment[1], y=y_end_segment+y_text_increase, label="SUPERIOR", 
            stat="identity", size=2.5, nudge_y = 0) +
  geom_text(x=x_segment[3], y=y_end_segment+y_text_increase, label="ANTERIOR", 
            stat="identity", size=2.5, nudge_y = 0, angle=90) +
  geom_text(x=x_segment[5], y=y_end_segment+y_text_increase, label="INFERIOR", 
            stat="identity", size=2.5, nudge_y = 0) +
  geom_text(x=x_segment[7], y=y_end_segment+y_text_increase, label="POSTERIOR", 
            stat="identity", size=2.5, nudge_y = 0, angle=90) +
  facet_grid(BMI ~ sex, switch="y") +
  coord_polar()

ggsave(p,
       file = paste("./figs/", "polar_ST_BMI_at_age_of_", age_value, ".pdf", sep=""),
       width = 6.5,
       height = 9,
       units = "in", device='pdf', dpi=500)
}