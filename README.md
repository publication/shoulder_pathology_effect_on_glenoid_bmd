# Glenoid bone mineral density (BMD) statistical analysis for CTRL, CTA and OA subjects study.

## Introduction
This repository contains R and Stan codes to build/import a bayesian network for assessing the effect of shoulder pathology (OA, CTA, and CTRL) on glenoid BMD, adjusting for age, sex, BMI, and CT kernel.

## Setup

### Download

Clone or download this repository.
Open shoulder_pathology_effect_on_glenoid_BMD.Rmd file.
Install the necessary libraries and run every section which saves the results in the fig directory.

## How to use
You need some R libraries for this code which you can see on the first section of the shoulder_pathology_effect_on_glenoid_BMD.Rmd file.

For rethinking package you need to install some libraries before.

Please follow the installation tutorial for this package here https://github.com/rmcelreath/rethinking


#### Access data

The necessary data for this study are located in the data directory.
There are two csv files, shoulder_glenoid_bmd.csv is the raw glenoid BMD calcaulations, and the shoulder_glenoid_bmd_model.csv is the transormed data suitable for the developed bayesian model.
If you want to use a ready model you can find one in zenodo. The model.rds is main model fitted for this study, and the prior_model.rds is the one fitted for prior predictive simulations.
You need to put this model in the data directory of this study repository.



#### Visualize data

All the graphs are saved automatically and you can change their names inside the glenoidBMD_CTRL_CTA_OA.Rmd file.


## Files/Folders

### Files
- age_BMI_effect_plot.R is a function which plots the BMD difference between two specified groups (OA, CTA, CTRL) while changing age or BMI.
- age_polar_plot.R is a function which generates the polar plots of glenoid BMD for OA-CTRL and CTA-CTRL differences for age of 50, 70, and 90 and a constant input BMI.
- age_table_for_patho.R is a function which saves a table of mean and 89% compatibility interval of BMD for each group at age of 50, 60, 70, 80, and 90.
- age_table.R is a function which saves a table of the mean and 89% compatibility interval of BMD differences between groups at age of 50, 60, 70, 80, and 90.
- BMI_polar_plot.R is a function which generates the polar plots of glenoid BMD for OA-CTRL and CTA-CTRL differences for BMI of 17, 25, and 32 and a constant input age.
- BMI_table_for_patho.R is a function which saves a table of mean and 89% compatibility interval of BMD for each group at BMI of 17, 22, 27, and 32.
- BMI_table.R is a function which saves a table of the mean and 89% compatibility interval of BMD differences between groups at BMI of 17, 22, 27, and 32.
- custom_link_without_kernel.R is a function which simulate the effect of the input variables on the outcome (bmd) based on the Bayesain model parameters. This function does not work with the mocdel including the kernel effect, as it is designed for prior predictive simulation which doesn't include kernel effect.
- custom_link.R is a function which simulate the effect of the input variables on the outcome (bmd) based on the Bayesain model parameters.
- kernel_effect_plot.R save plots of BMD simulations for specific constant inputs (sex, age, BMI) for the two different kernels.
- zscore_to_abs.R is a function which transforms back the standard BMD values to absolute based on each VOI mean and standard deviation.


### Folders
- **./data contains all of the necessary input data for the code and also the built model.
- **./fig contains all of the generated figures and tables from code.
- **./models contains the stan code of the Bayesian models.
- **./synthetic_data_simulation contains the generation and analysis of the synthetic data.


## Contributors
* Pezhman Eghbalishamsabadi, 2023-now.


## License

EPFL CC BY-NC-SA 4.0 Int.
