functions{
    vector merge_missing( array[] int miss_indexes , vector x_obs , vector x_miss ) {
        int N = dims(x_obs)[1];
        int N_miss = dims(x_miss)[1];
        vector[N] merged;
        merged = x_obs;
        for ( i in 1:N_miss )
            merged[ miss_indexes[i] ] = x_miss[i];
        return merged;
    }
}

data{
    int N;
    int N_BMI_missidx;
    int <lower=0, upper=1> priorOnly;
    vector[N] bmd;
    array[N] int kernel;
    array[N] int sex_group;
    array[N] int location;
    vector[N] BMI;
    array[N_BMI_missidx] int BMI_missidx;
    int age_mean;
    vector[N] age;
    array[N] int sex;
    real BMI_mean;
}

parameters{
    vector[2] aBMI;
    vector[2] bAgeBMI;
    real<lower=0> sigmaBMImodel;
    matrix[6,32] zAvg;
    matrix[6,32] zAge;
    matrix[6,32] zBMI;
    matrix[2,32] zKernel;
    
    vector<lower=0>[6] sigma_avg;
    cholesky_factor_corr[6] L_Rho_avg;
    
    vector<lower=0>[2] sigma_kernel;
    cholesky_factor_corr[2] L_Rho_kernel;
    
    vector<lower=0>[6] sigma_age;
    cholesky_factor_corr[6] L_Rho_age;
    
    vector<lower=0>[6] sigma_BMI;
    cholesky_factor_corr[6] L_Rho_BMI;
    
    real<lower=0> sigma;
    
    vector[N_BMI_missidx] BMI_impute;
}

transformed parameters{
    matrix[32,6] aAvg;
    matrix[32,6] bAge;
    matrix[32,6] bBMI;
    matrix[32,2] aKernel;
    
    bBMI = (diag_pre_multiply(sigma_BMI, L_Rho_BMI) * zBMI)';
    bAge = (diag_pre_multiply(sigma_age, L_Rho_age) * zAge)';
    aAvg = (diag_pre_multiply(sigma_avg, L_Rho_avg) * zAvg)';
    aKernel = (diag_pre_multiply(sigma_kernel, L_Rho_kernel) * zKernel)';
}

model{
    vector[N] mu;
    vector[N] BMI_merge;
    vector[N] muBMI;
    
    sigma ~ exponential( 2 );
    
    L_Rho_BMI ~ lkj_corr_cholesky( 2 );
    sigma_BMI ~ exponential( 10 );
    
    L_Rho_age ~ lkj_corr_cholesky( 2 );
    sigma_age ~ exponential( 10 );
    
    L_Rho_avg ~ lkj_corr_cholesky( 2 );
    sigma_avg ~ exponential( 2 );
    
    L_Rho_kernel ~ lkj_corr_cholesky( 2 );
    sigma_kernel ~ exponential( 2 );
    
    to_vector( zAvg ) ~ normal( 0 , 1 );
    to_vector( zAge ) ~ normal( 0 , 0.1 );
    to_vector( zBMI ) ~ normal( 0 , 0.1 );
    
    to_vector( zKernel ) ~ normal( 0 , 0.5 );
    
    //BMI model
    sigmaBMImodel ~ exponential( 0.2 );
    bAgeBMI ~ normal( 0 , 0.05 );
    aBMI ~ normal( BMI_mean , 2 );
    for ( i in 1:N ) {
        muBMI[i] = aBMI[sex[i]] + bAgeBMI[sex[i]] * (age[i] - age_mean);
    }
    BMI_merge = merge_missing(BMI_missidx, to_vector(BMI), BMI_impute);
    BMI_merge ~ normal( muBMI , sigmaBMImodel );
    
    //likelihood
    if (!priorOnly){
    
        for ( i in 1:N ) {
            mu[i] = aAvg   [location[i], sex_group[i]] +
                    aKernel[location[i], kernel   [i]] +
                    bAge   [location[i], sex_group[i]] * (age[i] - age_mean) +
                    bBMI   [location[i], sex_group[i]] * (BMI_merge[i] - BMI_mean);
        }
    
        bmd ~ normal(mu, sigma);
    }
    
}

generated quantities{
  vector[N] log_lik;
  vector[N] mu;
  vector[N] BMI_merge;
  vector[N] muBMI;
  matrix[6,6] Rho_avg;
  matrix[6,6] Rho_age;
  matrix[6,6] Rho_BMI;
  matrix[2,2] Rho_kernel;
  Rho_BMI = multiply_lower_tri_self_transpose(L_Rho_BMI);
  Rho_age = multiply_lower_tri_self_transpose(L_Rho_age);
  Rho_avg = multiply_lower_tri_self_transpose(L_Rho_avg);
  Rho_kernel = multiply_lower_tri_self_transpose(L_Rho_kernel);
  
  for ( i in 1:N ) {
      muBMI[i] = aBMI[sex[i]] + bAgeBMI[sex[i]] * (age[i] - age_mean);
  }
  BMI_merge = merge_missing(BMI_missidx, to_vector(BMI), BMI_impute);
  
  //likelihood
  if (!priorOnly){
  
  for ( i in 1:N ) {
      mu[i] = aAvg   [location[i], sex_group[i]] +
              aKernel[location[i], kernel   [i]] +
              bAge   [location[i], sex_group[i]] * (age[i] - age_mean) +
              bBMI   [location[i], sex_group[i]] * (BMI_merge[i] - BMI_mean);
  }
  
  for ( i in 1:N ) log_lik[i] = normal_lpdf( bmd[i] | mu[i] , sigma );
  }
  
}

