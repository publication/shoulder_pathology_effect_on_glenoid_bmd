data{
    int N;
    vector[N] bmd;
    array[N] int sex_patho;
    array[N] int location;
    vector[N] BMI;
    real age_mean;
    vector[N] age;
    real BMI_mean;
}
parameters{
    matrix[6,32] zAvg;
    matrix[6,32] zAge;
    matrix[6,32] zBMI;
    vector<lower=0>[6] sigma_avg;
    cholesky_factor_corr[6] L_Rho_avg;
    vector<lower=0>[6] sigma_age;
    cholesky_factor_corr[6] L_Rho_age;
    vector<lower=0>[6] sigma_BMI;
    cholesky_factor_corr[6] L_Rho_BMI;
    real<lower=0> sigma;
}
transformed parameters{
    matrix[32,6] aAvg;
    matrix[32,6] bAge;
    matrix[32,6] bBMI;
    bBMI = (diag_pre_multiply(sigma_BMI, L_Rho_BMI) * zBMI)';
    bAge = (diag_pre_multiply(sigma_age, L_Rho_age) * zAge)';
    aAvg = (diag_pre_multiply(sigma_avg, L_Rho_avg) * zAvg)';
}

model{
    vector[N] mu;
    sigma ~ exponential( 2 );
    L_Rho_BMI ~ lkj_corr_cholesky( 2 );
    sigma_BMI ~ exponential( 10 );
    L_Rho_age ~ lkj_corr_cholesky( 2 );
    sigma_age ~ exponential( 10 );
    L_Rho_avg ~ lkj_corr_cholesky( 2 );
    sigma_avg ~ exponential( 2 );
    
    to_vector( zAvg ) ~ normal( 0 , 1 );
    to_vector( zAge ) ~ normal( 0 , 0.1 );
    to_vector( zBMI ) ~ normal( 0 , 0.1 );
    
    //likelihood
    
    for ( i in 1:N ) {
        mu[i] = aAvg[location[i], sex_patho[i]] + bAge[location[i], sex_patho[i]] * (age[i] - age_mean) + bBMI[location[i], sex_patho[i]] * (BMI[i] - BMI_mean);
    }
    
    bmd ~ normal( mu , sigma );
    
    
}