##
model.synthesis.data.n1000 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 1000, noise_std = 1 , heterogeneous = F )$dd ,
  chains = 4 ,
  cores = 4 ,
  iter = 2000 
)

model.synthesis.data.n500 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 500, noise_std = 1 , heterogeneous = F )$dd ,
  chains = 4 ,
  cores = 4 ,
  iter = 2000 
)

model.synthesis.data.n300 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 300, noise_std = 1 , heterogeneous = F  )$dd ,
  chains = 4 ,
  cores = 4 ,
  iter = 2000 
)

model.synthesis.data.n100 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 100, noise_std = 1 , heterogeneous = F  )$dd ,
  chains = 4 ,
  cores = 4 ,
  iter = 2000 )

model.synthesis.data.n20 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 20, noise_std = 1 , heterogeneous = F  )$dd ,
  chains = 4 ,
  cores = 4 ,
  iter = 2000 )


##
model.synthesis.hdata.n1000 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 1000, noise_std = 1 , heterogeneous = T )$dd ,
  chains = 4 , 
  cores = 4 ,
  iter = 2000 
)

model.synthesis.hdata.n500 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 500, noise_std = 1 , heterogeneous = T )$dd ,
  chains = 4 ,
  cores = 4 ,
  iter = 2000 
)

model.synthesis.hdata.n300 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 300, noise_std = 1 , heterogeneous = T )$dd ,
  chains = 4 ,
  cores = 4 ,
  iter = 2000 
)

model.synthesis.hdata.n100 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 100, noise_std = 1 , heterogeneous = T )$dd ,
  chains = 4 ,
  cores = 4 ,
  iter = 2000 
)

model.synthesis.hdata.n20 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 20, noise_std = 1 , heterogeneous = T )$dd ,
  chains = 4 ,
  cores = 4 ,
  iter = 2000 
)

##
model.synthesis.hdata.N05.n1000 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 1000, noise_std = 1 , heterogeneous = T )$dd ,
  chains = 4 ,
  cores = 4 ,
  iter = 2000 
)

model.synthesis.hdata.N05.n500 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 500, noise_std = 0.5 , heterogeneous = T )$dd ,
  chains = 4 ,
  cores = 4 ,
  iter = 2000 
)

model.synthesis.hdata.N05.n300 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 300, noise_std = 0.5 , heterogeneous = T )$dd ,
  chains = 4 , 
  cores = 4 , 
  iter = 2000 
)

model.synthesis.hdata.N05.n100 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 100, noise_std = 0.5 , heterogeneous = T )$dd ,
  chains = 4 ,
  cores = 4 ,
  iter = 2000 
)

model.synthesis.hdata.N05.n20 <- stan( 
  "models/model_synthetic_data.stan" ,
  data = generate_data( n = 20, noise_std = 0.5 , heterogeneous = T )$dd ,
  chains = 4 ,
  cores = 4 , 
  iter = 2000 
)

post_n1000 <- extract.samples( model.synthesis.data.n1000 )
post_n500  <- extract.samples( model.synthesis.data.n500  )
post_n300  <- extract.samples( model.synthesis.data.n300  )
post_n100  <- extract.samples( model.synthesis.data.n100  )
post_n20   <- extract.samples( model.synthesis.data.n20   )

n1000 <- generate_data( n = 1000 , noise_std = 1 , heterogeneous = F)
n500  <- generate_data( n = 500  , noise_std = 1 , heterogeneous = F)
n300  <- generate_data( n = 300  , noise_std = 1 , heterogeneous = F)
n100  <- generate_data( n = 100  , noise_std = 1 , heterogeneous = F)
n20   <- generate_data( n = 20   , noise_std = 1 , heterogeneous = F)

ns <- list( n1000 , n500 , n300 , n100 , n20 )
posts <- list( post_n1000 , post_n500 , post_n300 , post_n100 , post_n20 )

recovered_parameters_plot(ns, posts, noise_std = 1, heterogeneous = F)
recovered_parameters_table(ns, posts, noise_std = 1, heterogeneous = F)

#
post_n1000 <- extract.samples( model.synthesis.hdata.n1000 )
post_n500  <- extract.samples( model.synthesis.hdata.n500  )
post_n300  <- extract.samples( model.synthesis.hdata.n300  )
post_n100  <- extract.samples( model.synthesis.hdata.n100  )
post_n20   <- extract.samples( model.synthesis.hdata.n20   )

n1000 <- generate_data( n = 1000, noise_std = 1 , heterogeneous = T)
n500  <- generate_data( n = 500 , noise_std = 1 , heterogeneous = T)
n300  <- generate_data( n = 300 , noise_std = 1 , heterogeneous = T)
n100  <- generate_data( n = 100 , noise_std = 1 , heterogeneous = T)
n20   <- generate_data( n = 20  , noise_std = 1 , heterogeneous = T)

ns <- list( n1000 , n500 , n300 , n100 , n20 )
posts <- list( post_n1000 , post_n500 , post_n300 , post_n100 , post_n20 )

recovered_parameters_plot(ns, posts, noise_std = 1 , heterogeneous = T)
recovered_parameters_table(ns, posts, noise_std = 1 , heterogeneous = T)

#
post_n1000 <- extract.samples( model.synthesis.hdata.N05.n1000 )
post_n500  <- extract.samples( model.synthesis.hdata.N05.n500  )
post_n300  <- extract.samples( model.synthesis.hdata.N05.n300  )
post_n100  <- extract.samples( model.synthesis.hdata.N05.n100  )
post_n20   <- extract.samples( model.synthesis.hdata.N05.n20   )

n1000 <- generate_data( n = 1000, noise_std = 0.5 , heterogeneous = T)
n500  <- generate_data( n = 500 , noise_std = 0.5 , heterogeneous = T)
n300  <- generate_data( n = 300 , noise_std = 0.5 , heterogeneous = T)
n100  <- generate_data( n = 100 , noise_std = 0.5 , heterogeneous = T)
n20   <- generate_data( n = 20  , noise_std = 0.5 , heterogeneous = T)

ns <- list( n1000 , n500 , n300 , n100 , n20 )
posts <- list( post_n1000 , post_n500 , post_n300 , post_n100 , post_n20 )

recovered_parameters_plot(ns, posts, noise_std = 0.5, heterogeneous = T)
recovered_parameters_table(ns, posts, noise_std = 0.5, heterogeneous = T)